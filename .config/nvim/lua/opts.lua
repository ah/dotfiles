local opt = vim.opt
local cmd = vim.cmd

vim.o.termguicolors = true

-- For nvim-metals (Scala)
vim.opt_global.shortmess:remove("F")

-- Editing
opt.encoding = "utf8"
opt.clipboard = "unnamedplus" -- use system's clipboard
opt.backspace = { "indent", "eol", "start" }
-- opt.complete- = i
opt.completeopt = { "menuone", "noinsert", "noselect" }
opt.modelines = 0
opt.modeline = false

opt.hidden = true -- allow unsaved background buffers and remember marks/undo for them
opt.linebreak = true -- don't display wrap
opt.textwidth = 80
opt.wrapmargin = 8
opt.startofline = false

opt.showmode = false
opt.cursorline = true
opt.guicursor = ""

opt.joinspaces = false -- use one space, not two, after punctuation

-- Errors
opt.errorbells = false
opt.visualbell = true
opt.confirm = true

-- opt.shortmess+ = c -- Don't pass messages to |ins-completion-menu|.
opt.shortmess = "atToOFc"

-- Folds
opt.foldenable = true
opt.foldmethod = "expr"
opt.foldexpr = "nvim_treesitter#foldexpr()"
opt.foldlevelstart = 99 -- open most folds by default
opt.foldnestmax = 10
opt.foldlevel = 1
-- opt.fillchars = fold:\ ,

-- Search and replace
opt.hlsearch = false -- don't highlight search results
opt.smartcase = true -- ignore case when searching lowercase
opt.incsearch = true -- incremental search, search as you type
opt.gdefault = true -- use 'g' by default for substitutions, type 'g' for only one
opt.inccommand = "nosplit"

-- tab and indent
opt.autoindent = true
opt.expandtab = true -- insert spaces when tab is pressed
opt.wrap = false -- don't wrap text
opt.shiftwidth = 4
opt.smartindent = true
opt.smarttab = true
opt.shiftround = true
opt.tabstop = 4 -- one tab == four spaces
opt.softtabstop = 4

-- Interface
opt.showbreak = "↪"
opt.showmatch = true -- show matching bracket
opt.cmdheight = 1
-- opt.diffopt = vertical -- diffs are shown side-by-side, not above/below
-- opt.display+ = lastline
opt.lazyredraw = false
opt.numberwidth = 1
opt.previewheight = 30
opt.ruler = true -- always show current position
opt.scrolloff = 4 -- alway show some lines above/below cursor
opt.showcmd = true
opt.sidescrolloff = 5
opt.termguicolors = true -- enable true colors = true
opt.title = true
opt.signcolumn = "yes"
opt.updatetime = 250
opt.ttyfast = true -- faster redrawing
table.insert(opt.diffopt, "vertical")
table.insert(opt.diffopt, "iwhite")
table.insert(opt.diffopt, "internal")
table.insert(opt.diffopt, "algorithm:patience")
table.insert(opt.diffopt, "hiddenoff")

-- -- Resize split when window is resized
-- autocmd VimResized * :wincmd  =

-- case-insensitive tab-completion
opt.wildmenu = true
opt.wildmode = { "list:longest", "list:full" }
opt.wildignorecase = true
opt.wildoptions = "pum"
opt.showfulltag = true

opt.winblend = 0 -- enable pseudo-transparency for floating windows
opt.pumblend = 10 -- enable pseudo-transparency for popups

opt.colorcolumn = "80"

cmd([[highlight ColorColumn ctermbg = DarkGray]])

-- Show relative numbers in normal mode and absolute number in insertion mode
opt.number = true -- show line numbers
opt.relativenumber = true
cmd([[
augroup toggle_relative_number
    autocmd InsertEnter * :setlocal norelativenumber
    autocmd InsertLeave * :setlocal relativenumber
augroup End
]])

-- enable mouse in normal and visual modes
opt.mouse = "a"

cmd([[
filetype plugin indent on
syntax enable
]])

-- Swaps and backups
opt.swapfile = false -- don't create swap file, 'cause I save often and use tmux
opt.backup = false -- don't make backup
opt.autowrite = true -- automatically :write before running some commands
opt.autoread = true -- autoread when file has been changed outside of Vim
cmd([[au FocusGained,BufEnter * :checktime]])

-- Undo
opt.undolevels = 1000
-- Use persistent undo history.
cmd([[
if !isdirectory("/tmp/.vim-undo-dir")
    call mkdir("/tmp/.vim-undo-dir", "", 0700)
endif
]])
opt.undodir = "/tmp/.vim-undo-dir"
opt.undofile = true

-- timeout
opt.ttimeout = true
opt.timeoutlen = 300
opt.ttimeoutlen = 100

-- Display extra whitespace
opt.list = false
-- opt.listchars = {
-- 	tab = "▏",
-- 	trail = "·",
-- 	precedes = "←",
-- 	extends = "→",
-- 	eol = "↲",
-- 	nbsp = "␣"
-- }

-- Make vertical separator pretty
cmd([[highlight VertSplit cterm=None ]])
opt.fillchars = {
    vert = "▕", -- alternatives │
    fold = " ",
    eob = " ", -- suppress ~ at EndOfBuffer
    diff = "╱", -- alternatives = ⣿ ░ ─
    msgsep = "‾",
    foldopen = "▾",
    foldsep = "│",
    foldclose = "▸",
}

opt.history = 1000
opt.tabpagemax = 50

-- Delete comment character when joining commented lines
opt.formatoptions = opt.formatoptions + "j"

-- open new split panes to right right and bottom
opt.splitbelow = true
opt.splitright = true

opt.laststatus = 0

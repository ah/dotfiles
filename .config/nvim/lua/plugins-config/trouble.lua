require("trouble").setup({
	position = "bottom", -- position of the list can be: bottom, top, left, right
	height = 8, -- height of the trouble list when position is top or bottom
	width = 50, -- width of the list when position is left or right
	icons = true, -- use devicons for filenames
	mode = "workspace_diagnostics", -- "workspace_diagnostics", "document_diagnostics", "quickfix",
})

local nmap = require("utils").nmap
-- trouble keymappings
nmap("<leader>bx", "<cmd>TroubleToggle<cr>")
nmap("<leader>bw", "<cmd>TroubleToggle workspace_diagnostics<cr>")
nmap("<leader>bd", "<cmd>TroubleToggle document_diagnostics<cr>")
nmap("<leader>bq", "<cmd>TroubleToggle quickfix<cr>")
nmap("<leader>bl", "<cmd>TroubleToggle loclist<cr>")
-- nnoremap("gR", "<cmd>TroubleToggle lsp_references<cr>")

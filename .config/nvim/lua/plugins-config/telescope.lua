local actions = require("telescope.actions")

require("telescope").setup({
	defaults = {
		color_devicons = true,
		disable_devicons = false,

		prompt_prefix = "    ",
		selection_caret = "  ",
		entry_prefix = "  ",

		initial_mode = "insert",
		selection_strategy = "reset",
		sorting_strategy = "ascending",
		layout_strategy = "horizontal",
		layout_config = {
			horizontal = {
				prompt_position = "top",
				preview_width = 0.55,
				results_width = 0.8,
			},
			vertical = {
				prompt_position = "top",
				mirror = true,
			},
			width = 0.8,
			height = 0.80,
			preview_cutoff = 120,
		},
		winblend = 3,

		mappings = {
			i = {
				["<C-j>"] = actions.move_selection_next,
				["<C-k>"] = actions.move_selection_previous,
				["<esc>"] = actions.close,
			},
		},
	},
	pickers = {
		file_browser = {
			disable_devicons = false,
		},
		buffers = {
			sort_lastused = true,
			ignore_current_buffer = true,
			sort_mru = true,
		}
	},
})

-- Enable telescope fzf native
require("telescope").load_extension("fzf")

vim.keymap.set("n", "<leader>ff", require("telescope.builtin").find_files)
vim.keymap.set("n", "<C-p>", require("telescope.builtin").find_files)
vim.keymap.set("n", "<leader>fg", require("telescope.builtin").live_grep)
vim.keymap.set("n", "<leader>fb", require("telescope.builtin").buffers)
vim.keymap.set("n", "<leader>fh", require("telescope.builtin").help_tags)
vim.keymap.set("n", "<leader>fo", require("telescope.builtin").oldfiles)
vim.keymap.set("n", "<leader>fi", require("telescope.builtin").diagnostics)

-- nnoremap <leader>fs :Telescope coc workspace_symbols<cr>
-- nnoremap <leader>fd :Telescope coc document_symbols<cr>

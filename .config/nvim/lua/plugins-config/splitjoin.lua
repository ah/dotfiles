local g = vim.g

g["splitjoin_split_mapping"] = "S"
g["splitjoin_join_mapping"] = "J"

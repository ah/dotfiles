require("lualine").setup({
	options = {
		icons_enabled = true,
		theme = "auto",
		component_separators = "",
		section_separators = { left = "", right = "" },
		-- section_separators = "",
		disabled_filetypes = {},
		globalstatus = false,
		divide_middle = true,
	},
	sections = {
		lualine_a = {
			{ "mode", separator = { left = "" }, right_padding = 2 },
		},
		lualine_b = {
			{ "filename", file_status = true, path = 0 }
		},
		lualine_c = {
			{ 'diagnostics', sources = { "nvim_diagnostic" }, symbols = { error = ' ', warn = ' ', info = ' ',
				hint = ' ' } },
			"diff"
		},
		lualine_x = {
			"branch",
		},
		lualine_y = { "filetype" },
		lualine_z = {
			{ "location", separator = { right = "" }, left_padding = 2 },
		},
	},
	inactive_sections = {
		lualine_a = {},
		lualine_b = {},
		lualine_c = { { "filename", file_status = true, path = 1 } },
		lualine_x = {},
		lualine_y = {},
		lualine_z = {},
	},
	winbar = {},
	inactive_winbar = {},
	tabline = {},
	extensions = { "fugitive" },
})

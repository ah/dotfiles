vim.g.user_emmet_expandabbr_key = "<Tab>"
vim.g.user_emmet_install_global = 0
vim.cmd([[autocmd FileType html,css,elixir EmmetInstall]])
vim.g.emmet_html5 = 1

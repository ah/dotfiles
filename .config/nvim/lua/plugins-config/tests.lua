local g = vim.g
local v = vim.v
local nmap = require("utils").nmap

g["test#preserve_screen"] = false
g["test#javascript#jest#options"] = "--reporters jest-vim-reporter"
g["test#filename_modifier"] = ":p"
g["test#neovim#term_position"] = "vert"
g["test#preserve_screen"] = 1
g["test#strategy"] = "dispatch"

nmap("<leader>tn", ":TestNearest<CR>")
nmap("<leader>tf", ":TestFile<CR>")
nmap("<leader>ta", ":TestSuite<CR>")
nmap("<leader>tl", ":TestLast<CR>")
nmap("<leader>tv", ":TestVisit<CR>")

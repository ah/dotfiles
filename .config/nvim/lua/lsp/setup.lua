local icons = require("icons")

local lspconfig = require("lspconfig")
local handlers = require("lsp.handlers")

local status, mason = pcall(require, "mason")
if (not status) then
	return
end

local status2, mason_lspconfig = pcall(require, "mason-lspconfig")
if (not status2) then
	return
end

mason.setup({})

-- Enable the following language servers
local servers = {
	"clangd",
	"cssls",
	"dockerls",
	"elixirls",
	"html",
	"jsonls",
	"marksman",
	"pylsp",
	"rust_analyzer",
	"tailwindcss",
	"texlab",
	"tsserver",
}

mason_lspconfig.setup {
	ensure_installed = servers,
}

for _, lsp in ipairs(servers) do
	if lsp ~= "tsserver" and lsp ~= "tailwindcss" and lsp ~= "rust_analyzer" and lsp ~= "elixirls" then
		lspconfig[lsp].setup({
			on_attach = handlers.on_attach,
			capabilities = handlers.capabilities,
		})
	end
end

lspconfig.elixirls.setup({
	on_attach = handlers.on_attach,
	capabilities = handlers.capabilities,
	flags = {
		debounce_text_changes = 150,
	},
	filetypes = { "elixir", "eelixir", "heex" },
	elixirLS = {
		dialyzerEnabled = true,
		fetchDeps = false,
		enableTestLenses = true,
	},
})

require("elixir").setup()

-- lspconfig.efm.setup({
-- 	-- cmd = { "/home/ah/go/bin/efm-langserver" },
-- 	on_attach = handlers.on_attach,
-- 	capabilities = handlers.capabilities,
-- 	filetypes = { "elixir" }
-- })

lspconfig.rust_analyzer.setup({
	on_attach = handlers.on_attach,
	capabilities = handlers.capabilities,
	flags = {
		debounce_text_changes = 150,
	},
})

lspconfig.tailwindcss.setup({
	on_attach = handlers.on_attach,
	capabilities = handlers.capabilities,
	experimental = {
		classRegex = {
			[[class="([^"]*)]],
			"class=\\s+\"([^\"]*)"
		},
	},
	init_options = {
		userLanguages = {
			html = "html",
			heex = "phoenix-heex",
			elixir = "html-eex",
			eelixir = "html-eex",
			eruby = "erb"
		}
	},
	settings = {
		tailwindCSS = {
			classAttributes = { "class", "className", "classList", "ngClass" },
			lint = {
				cssConflict = "warning",
				invalidApply = "error",
				invalidConfigPath = "error",
				invalidScreen = "error",
				invalidTailwindDirective = "error",
				invalidVariant = "error",
				recommendedVariantOrder = "warning"
			},
			validate = true
		}

	}
})

lspconfig.tsserver.setup {
	on_attach = handlers.on_attach,
	filetypes = { "javascript", "typescript", "typescriptreact", "typescript.tsx" },
	cmd = { "typescript-language-server", "--stdio" }
}

-- Make runtime files discoverable to the server
local runtime_path = vim.split(package.path, ";")
table.insert(runtime_path, "lua/?.lua")
table.insert(runtime_path, "lua/?/init.lua")

lspconfig.lua_ls.setup({
	on_attach = handlers.on_attach,
	capabilities = handlers.capabilities,
	settings = {
		Lua = {
			runtime = {
				-- Tell the language server which version of Lua you're using (most likely LuaJIT in the case of Neovim)
				version = "LuaJIT",
				-- Setup your lua path
				path = runtime_path,
			},
			diagnostics = {
				-- Get the language server to recognize the `vim` global
				globals = { "vim" },
			},
			workspace = {
				-- Make the server aware of Neovim runtime files
				library = vim.api.nvim_get_runtime_file("", true),
			},
			-- Do not send telemetry data containing a randomized but unique identifier
			telemetry = {
				enable = false,
			},
		},
	},
})

-- luasnip setup
local luasnip = require("luasnip")

-- nvim-cmp setup
local cmp = require("cmp")
local lspkind = require('lspkind')
cmp.setup({
	formatting = {
		format = lspkind.cmp_format({
			mode = 'symbol_text',
			maxwidth = 50, -- prevent the popup from showing more than provided characters (e.g 50 will not show more than 50 characters)
			ellipsis_char = '...', -- when popup menu exceed maxwidth, the truncated part would show ellipsis_char instead (must define maxwidth first)
			with_text = false
		})
	},
	snippet = {
		expand = function(args)
			luasnip.lsp_expand(args.body)
		end,
	},
	mapping = cmp.mapping.preset.insert({
		["<C-d>"] = cmp.mapping.scroll_docs( -4),
		["<C-f>"] = cmp.mapping.scroll_docs(4),
		["<C-e>"] = cmp.mapping.close(),
		["<C-Space>"] = cmp.mapping.complete(),
		["<CR>"] = cmp.mapping.confirm({
			behavior = cmp.ConfirmBehavior.Replace,
			select = true,
		}),
		["<Tab>"] = cmp.mapping(function(fallback)
			if cmp.visible() then
				cmp.select_next_item()
			elseif luasnip.expand_or_jumpable() then
				luasnip.expand_or_jump()
			else
				fallback()
			end
		end, { "i", "s" }),
		["<S-Tab>"] = cmp.mapping(function(fallback)
			if cmp.visible() then
				cmp.select_prev_item()
			elseif luasnip.jumpable( -1) then
				luasnip.jump( -1)
			else
				fallback()
			end
		end, { "i", "s" }),
	}),
	sources = {
		{ name = "nvim_lsp" },
		{ name = "luasnip" },
	},
})

local signs = {
	Error = icons.error .. " ",
	Warn = icons.warn .. " ",
	Hint = icons.hint .. " ",
	Info = icons.info .. " ",
}

for type, icon in pairs(signs) do
	local hl = "DiagnosticSign" .. type
	vim.fn.sign_define(hl, { text = icon, texthl = hl, numhl = "" })
end

-- lspconfig.diagnosticls.setup({
-- 	underline = true,
-- 	signs = true,
-- 	update_in_insert = false,
-- 	severity_sort = true,
-- 	float = {
-- 		border = "rounded",
-- 		focusable = false,
-- 		header = { icons.debug .. " Diagnostics:", "Normal" },
-- 		source = "always",
-- 	},
-- 	virtual_text = {
-- 		spacing = 4,
-- 		source = "always",
-- 		severity = {
-- 			min = vim.diagnostic.severity.HINT,
-- 		},
-- 	},
-- 	on_attach = handlers.on_attach,
-- 	filetypes = {
-- 		"javascript",
-- 		"javascriptreact",
-- 		"json",
-- 		"typescript",
-- 		"typescriptreact",
-- 		"css",
-- 		"less",
-- 		"scss",
-- 		"pandoc",
-- 	},
-- 	init_options = {
-- 		linters = {
-- 			eslint = {
-- 				command = "eslint_d",
-- 				rootPatterns = { ".git" },
-- 				debounce = 100,
-- 				args = { "--stdin", "--stdin-filename", "%filepath", "--format", "json" },
-- 				sourceName = "eslint_d",
-- 				parseJson = {
-- 					errorsRoot = "[0].messages",
-- 					line = "line",
-- 					column = "column",
-- 					endLine = "endLine",
-- 					endColumn = "endColumn",
-- 					message = "[eslint] ${message} [${ruleId}]",
-- 					security = "severity",
-- 				},
-- 				securities = {
-- 					[2] = "error",
-- 					[1] = "warning",
-- 				},
-- 			},
-- 		},
-- 		filetypes = {
-- 			javascript = "eslint",
-- 			javascriptreact = "eslint",
-- 			typescript = "eslint",
-- 			typescriptreact = "eslint",
-- 		},
-- 		formatters = {
-- 			eslint_d = {
-- 				command = "eslint_d",
-- 				rootPatterns = { ".git" },
-- 				args = { "--stdin", "--stdin-filename", "%filename", "--fix-to-stdout" },
-- 			},
-- 			prettier = {
-- 				command = "prettier_d_slim",
-- 				rootPatterns = { ".git" },
-- 				-- requiredFiles: { 'prettier.config.js' },
-- 				args = { "--stdin", "--stdin-filepath", "%filename" },
-- 			},
-- 		},
-- 		formatFiletypes = {
-- 			css = "prettier",
-- 			javascript = "prettier",
-- 			javascriptreact = "prettier",
-- 			json = "prettier",
-- 			scss = "prettier",
-- 			less = "prettier",
-- 			typescript = "prettier",
-- 			typescriptreact = "prettier",
-- 		},
-- 	},
-- })

-- icon
vim.lsp.handlers["textDocument/publishDiagnostics"] = vim.lsp.with(vim.lsp.diagnostic.on_publish_diagnostics, {
		underline = true,
		-- This sets the spacing and the prefix, obviously.
		virtual_text = {
			spacing = 4,
			prefix = "",
		},
	})


local function enable_format_on_save()
	vim.cmd([[
    augroup format_on_save
    au!
    au BufWritePre *\(.scala\)\@<! lua vim.lsp.buf.format(nil, 2000)
    augroup end
    ]])
end

enable_format_on_save()

-- Borders.
-- vim.cmd [[autocmd! ColorScheme * highlight NormalFloat guibg=#1f2335]]
-- vim.cmd [[autocmd! ColorScheme * highlight FloatBorder guifg=white guibg=#1f2335]]

-- handlers["textDocument/hover"] = vim.lsp.with(vim.lsp.handlers.hover, {
-- 		border = "rounded",
-- 	})

-- handlers["textDocument/signatureHelp"] = vim.lsp.with(vim.lsp.handlers.signature_help, {
-- 		border = "rounded",
-- 	})

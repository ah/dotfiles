local status_ok, saga = pcall(require, "lspsaga")
if not status_ok then
	return
end

local icons = require("icons")

saga.init_lsp_saga({
	border_style = "round",
	error_sign = icons.error,
	warn_sign = icons.warn,
	hint_sign = icons.hint,
	infor_sign = icons.info,
	code_action_icon = icons.hint,
	-- code_action_num_shortcut = true,
	-- code_action_lightbulb = {
	-- 	enable = true,
	-- 	enable_in_insert = true,
	-- 	cache_code_action = true,
	-- 	sign = false,
	-- 	update_time = 150,
	-- 	sign_priority = 20,
	-- 	virtual_text = true,
	-- },
})

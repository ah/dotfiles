local map = require("utils").map
local nmap = require("utils").nmap
local vmap = require("utils").vmap
local imap = require("utils").imap

vim.g.mapleader = " "

map("", "<F7>", ":tabp<CR>")
map("", "<F8>", ":tabn<CR>")

-- center screen after vertical move
nmap("<C-d>", "<C-d>zz")
nmap("<C-u>", "<C-u>zz")

-- U is useless (except for Vi compatibility), make redo instead
nmap("U", "<C-r>")

-- don't use Ex mode
nmap("Q", "<nop>")

-- switch search highlight
nmap("<leader>h", ":set hlsearch!<cr>")

-- use tab to navigate curly braces instead of %
nmap("<tab>", "%")
vmap("<tab>", "%")

-- light and dark theme
nmap("<leader>cd", ":set background=dark<cr>")
nmap("<leader>cl", ":set background=light<cr>")

-- use leader key instead of control to navigate to prev/next position
nmap("<leader>o", "<c-o>")
nmap("<leader>i", "<c-i>")

-- In insert mode, stop f*** your pinky hitting Esc all the time
imap("jk", "<esc>")
-- Also in visual mode!
vmap("ii", "<esc>")
-- use ;; to escape everywhere
map("", ";;", "<esc>")
-- save file
nmap("<leader>s", ":w<cr>")
-- close buffer
nmap("<leader>w", ":Bdelete<cr>")
-- switch to last buffer
nmap("<leader><leader>", "<c-^>")
-- reload config file
nmap("<leader>x", ":source ~/.config/nvim/init.lua<cr>")

-- nmap("<C-f>", "<cmd>silent !tmux neww tmux-sessionizer<CR>")

-- easy window navigation
map("", "<C-h>", "<C-w>h")
map("", "<C-j>", "<C-w>j")
map("", "<C-k>", "<C-w>k")
map("", "<C-l>", "<C-w>l")

map("n", "sh", ":split<Return><C-w>j")
map("n", "sv", ":vsplit<Return><C-w>l")

-- make Y behave like D
nmap("Y", "y$")

-- keep cursor centered when searching and unfold
nmap("n", "nzzzv")
nmap("N", "nzNzzzv")

-- keep cursor position when joining lines
nmap("J", "mzJ`z")

-- move lines and indent
map("n", "<leader>j", ":m .+1<cr>==", { silent = true })
map("n", "<leader>k", ":m .-2<cr>==", { silent = true })
map("v", "J", ":m '>+1<cr>gv=gv", { silent = true })
map("v", "K", ":m '<-2<cr>gv=gv", { silent = true })

-- disable arrow keys for navigation and use them for resizing
nmap("<Down>", ":resize -2<cr>")
nmap("<Left>", ":vertical resize +2<cr>")
nmap("<Right>", ":vertical resize -2<cr>")
nmap("<Up>", ":resize +2<cr>")

-- Sort lines, selected or over motion.
map("x", "gs", ":sort i<CR>", { silent = true })
map("v", "gs", ":set opfunc=SortLines<CR>g@", { silent = true })
vim.cmd([[
	fun! SortLines(type) abort
	'[,']sort i
	endfun
]])

-- Install packer
local install_path = vim.fn.stdpath("data") .. "/site/pack/packer/start/packer.nvim"

if vim.fn.empty(vim.fn.glob(install_path)) > 0 then
	vim.fn.execute("!git clone https://github.com/wbthomason/packer.nvim " .. install_path)
end

local packer_group = vim.api.nvim_create_augroup("Packer", { clear = true })
vim.api.nvim_create_autocmd(
	"BufWritePost",
	{ command = "source <afile> | PackerCompile", group = packer_group, pattern = "init.lua" }
)

require("packer").startup(function(use)
	use("wbthomason/packer.nvim") -- Package manager

	-- Utilities
	use("tpope/vim-sensible") -- defaults everyone can agree on
	use("tpope/vim-commentary") -- easy commenting
	use("tpope/vim-surround") -- easily find and replace surrounding
	use("tpope/vim-repeat") -- superchage .-command
	use("tpope/vim-projectionist") -- alternate between file and test file
	use("tpope/vim-sleuth") -- Heuristically set buffer indent options
	use("kevinhwang91/nvim-bqf") -- better quickfix window
	use("AndrewRadev/splitjoin.vim") -- switch between single- and multi-line
	use("kyazdani42/nvim-web-devicons") -- for file icons
	use("nvim-lua/plenary.nvim") -- lua helpers used by other plugins
	use("moll/vim-bbye") -- :Bdelete buffer w/o closing window
	-- file explorer
	use({ "nvim-neo-tree/neo-tree.nvim", requires = { "MunifTanjim/nui.nvim" } })

	-- UI to select things (files, grep results, open buffers...)
	use({ "nvim-telescope/telescope.nvim", requires = { "nvim-lua/plenary.nvim" } })
	use({ "nvim-telescope/telescope-fzf-native.nvim", run = "make" })

	use({
		"phaazon/hop.nvim",
		branch = "v2", -- optional but strongly recommended
		config = function()
			-- you can configure Hop the way you like here; see :h hop-config
			require("hop").setup({ keys = "etovxqpdygfblzhckisuran" })
		end,
	})
	use('windwp/nvim-spectre') -- VS Code-like find and replace

	-- Term
	use { "akinsho/toggleterm.nvim", tag = '*', config = function()
		require("toggleterm").setup()
	end }

	-- use({
	-- 	"folke/noice.nvim",
	-- 	requires = {
	-- 		-- if you lazy-load any plugin below, make sure to add proper `module="..."` entries
	-- 		"MunifTanjim/nui.nvim",
	-- 		-- OPTIONAL:
	-- 		--   `nvim-notify` is only needed, if you want to use the notification view.
	-- 		--   If not available, we use `mini` as the fallback
	-- 		-- "rcarriga/nvim-notify",
	-- 	}
	-- })

	-- Add indentation guides even on blank lines
	-- use("lukas-reineke/indent-blankline.nvim")

	-- Highlight, edit, and navigate code using a fast incremental parsing library
	use({ "nvim-treesitter/nvim-treesitter", run = ":TSUpdate" })
	use("nvim-treesitter/playground") -- show treesitter nodes

	-- Additional textobjects for treesitter, e.g. functions and classes
	use("nvim-treesitter/nvim-treesitter-textobjects")

	-- LSP
	use {
		"williamboman/mason.nvim",
		"williamboman/mason-lspconfig.nvim",
		"neovim/nvim-lspconfig",
	}

	use("mattn/efm-langserver")

	-- cmp plugins
	use("hrsh7th/nvim-cmp") -- Autocompletion plugin
	use("hrsh7th/cmp-buffer")
	use("hrsh7th/cmp-path")
	use("hrsh7th/cmp-cmdline")
	use("hrsh7th/cmp-nvim-lsp")
	use("simrat39/symbols-outline.nvim")
	use("saadparwaiz1/cmp_luasnip") -- snippets completion
	use("tami5/lspsaga.nvim")

	-- Snippets
	use("L3MON4D3/LuaSnip") -- Snippets plugin

	-- Generic programming helpers
	-- use {
	-- 	"windwp/nvim-autopairs",
	-- 	config = function() require("nvim-autopairs").setup {} end
	-- }
	use("windwp/nvim-ts-autotag") -- use treesitter to autoclose and autorename html tags
	use("tpope/vim-endwise") -- automatically add end closings
	-- use("folke/trouble.nvim") -- pretty list for diagnostics
	-- use("liuchengxu/vista.vim")

	-- Git
	use("tpope/vim-fugitive")
	use("shumphrey/fugitive-gitlab.vim")
	-- Add git related info in the signs columns and popups
	use({ "lewis6991/gitsigns.nvim", requires = { "nvim-lua/plenary.nvim" } })

	-- Interface
	use("onsails/lspkind-nvim") -- show nerd fonts in lsp completion
	use("nvim-lualine/lualine.nvim")
	use("edkolev/tmuxline.vim") -- generate tmux statusline
	use("christoomey/vim-tmux-navigator")

	-- Themes
	use("projekt0n/github-nvim-theme")
	use("sainnhe/sonokai") -- nice monokai-like theme
	use("overcache/NeoSolarized")
	use("navarasu/onedark.nvim")
	use('kvrohit/mellow.nvim')
	use("rebelot/kanagawa.nvim")
	use("shaunsingh/solarized.nvim")
	use("morhetz/gruvbox")
	use("drewtempelmeyer/palenight.vim")
	use("catppuccin/nvim")

	-- Debugging
	use { "rcarriga/nvim-dap-ui", requires = { "mfussenegger/nvim-dap" } }

	-- Tests
	use("vim-test/vim-test") -- wrapper for running tests on different granularities
	use("tpope/vim-dispatch") -- asynchronous build and test dispatcher
	-- use "rcarriga/vim-ultest", { "do": ":UpdateRemotePlugins" }
	use {
		"nvim-neotest/neotest",
		requires = {
			"nvim-lua/plenary.nvim",
			"nvim-treesitter/nvim-treesitter",
			"antoinemadec/FixCursorHold.nvim",
			"jfpedroza/neotest-elixir",
		}
	}

	-- Elixir
	use("elixir-editors/vim-elixir")
	use({ "elixir-tools/elixir-tools.nvim", tag = "stable", requires = { "nvim-lua/plenary.nvim" }})
	-- use({ "mhanberg/elixir.nvim", requires = { "neovim/nvim-lspconfig", "nvim-lua/plenary.nvim" } })
	-- use "spiegela/vimix" -- elixir support for tmux

	-- Rust
	use("rust-lang/rust.vim")

	-- Markdown and writing
	use("tpope/vim-markdown")
	-- install without yarn or npm
	-- use({
	-- 	"iamcco/markdown-preview.nvim",
	-- 	run = function() vim.fn["mkdp#util#install"]() end,
	-- })
	use({
		"iamcco/markdown-preview.nvim",
		run = "cd app && npm install",
		setup = function() vim.g.mkdp_filetypes = { "markdown" } end,
		ft = { "markdown" },
	})

	-- LaTeX
	use("lervag/vimtex")

	-- HTML and CSS
	use("norcalli/nvim-colorizer.lua") -- add color to hex values
	use("mattn/emmet-vim") -- emmet support
	-- use "tpope/vim-ragtag" " endings for html, xml
	-- use "gregsexton/MatchTag", { "for": "html" } " match tags in html
	-- use "othree/html5.vim", { "for": "html" } " html5 support
	-- use "neoclide/jsonc.vim"
	-- use "ludovicchabant/vim-gutentags" -- Automatic tags management

	-- Scala
	use({
		"scalameta/nvim-metals",
		requires = {
			"nvim-lua/plenary.nvim",
			"mfussenegger/nvim-dap",
		},
	})
end)

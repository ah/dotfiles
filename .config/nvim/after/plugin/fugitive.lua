vim.keymap.set("n", "<leader>gg", vim.cmd.Git, { silent = true })
vim.keymap.set("n", "<leader>gp", ":G push<CR>", { silent = true })
vim.keymap.set("n", "<leader>gf", ":G fetch<CR>", { silent = true })
vim.keymap.set("n", "<leader>gb", ":G co -b<CR>", { silent = true })

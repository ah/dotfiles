require("plugins")

require("opts")
require("keymaps")
require("misc")

require("plugins-config")
require("lsp")

-- require("startup").setup({ theme = "dashboard" })
require("colorscheme")
require("colorizer").setup()

-- vim: ts=2 sts=2 sw=2 et

export QTPATH=$HOME/Qt/5.15.2/gcc_64/bin:$HOME/bin/Qt/Tools/QtCreator/bin/
export PATH=$HOME/.local/bin:$HOME/bin:/usr/local/bin:$PATH:$QTPATH
export PATH=$PATH:$HOME/.lsp/lua/bin
export PATH=$PATH:$HOME/bin/node/bin
export PATH=$HOME/.npm-global/bin:$PATH
export PATH=$PATH:$HOME/.local/share/nvim/mason/bin/
export PATH=$PATH:$HOME/.cargo/bin
export JAVA_HOME=/usr/lib/jvm/java-11-openjdk-11.0.18.0.10-1.fc37.x86_64/

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
fi

export ZSH=/home/ah/.oh-my-zsh
export ANSIBLE_NOCOWS=1

export FLYCTL_INSTALL="/home/ah/.fly"
export PATH="$FLYCTL_INSTALL/bin:$PATH"

# Enable IEX shell history
export ERL_AFLAGS="-kernel shell_history enabled"

set editing-mode vi

eval "$(starship init zsh)"

HIST_STAMPS="dd/mm/yyyy"

export USE_CCACHE=1
export CCACHE_EXEC=/usr/bin/ccache

plugins=(
    asdf
    dnf
    git
    ssh-agent
    sudo
    vi-mode
    zsh-autosuggestions
)

source $ZSH/oh-my-zsh.sh

# User configuration
# ssh
export SSH_KEY_PATH="~/.ssh/rsa_id"

# added by travis gem
[ -f /home/ah/.travis/travis.sh ] && source /home/ah/.travis/travis.sh

export PATH="$HOME/.poetry/bin:$PATH"

# ASDF
# . $HOME/.asdf/asdf.sh

# Neovim
export EDITOR=nvim
export VISUAL=nvim
export VIMCONFIG=~/.config/nvim
export VIMDATA=~/.local/share/nvim
alias vi="nvim"
alias vim="nvim"
alias vimdiff="nvim -d"
alias e="$EDITOR"
alias v="$VISUAL"
alias mkdir="mkdir -p"

alias bjson="python -m json.tool|pygmentize -l json"

if [[ -x `which bat` ]]; then
    alias cat="bat"
fi

if [[ -x `which exa` ]]; then
    alias ls="exa"
    alias ll="exa -al"
    alias l="exa -al"
fi

if [[ -x `which htop` ]]; then
    alias top="htop"
fi

if [[ -x `which git` ]]; then
    # Git alias for dotfiles
    # See https://www.atlassian.com/git/tutorials/dotfiles for more info.
    alias config="/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME"
fi

# Elixir
alias iem="iex -S mix"

# Tmux
alias tdev="sh ~/.bin/tmux-dev.sh"
alias t="tmux"
alias ta="t a -t"
alias tls="t ls"
alias tn="t new -t"

fh() {
  print -z $( ([ -n "$ZSH_NAME" ] && fc -l 1 || history) | fzf +s --tac | sed -E 's/ *[0-9]*\*? *//' | sed -E 's/\\/\\\\/g')
}
